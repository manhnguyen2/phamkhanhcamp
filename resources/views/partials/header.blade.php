    
        <header class="main-header-two">
            <nav class="main-menu">
                <div class="container">
                    <div class="main-menu__logo">
                        <a href="index.html">
                            <img src="{{ asset('phamkhanhcamp/assets/images/logo-two.png')}}" width="183" height="48" alt="Eduact">
                        </a>
                    </div><!-- /.main-menu__logo -->
                    <div class="main-menu__nav">
                        <ul class="main-menu__list">
                            <li class="megamenu megamenu-clickable megamenu-clickable--toggler">
                                <a href="#">Home</a>
                                <ul class="">
                                    <li>
                                        <div class="megamenu-popup">
                                            <a href="#" class="megamenu-clickable--close"><span class="icon-close"></span></a><!-- /.megamenu-clickable--close -->
                                            <div class="megamenu-popup__content">
                                                <div class="demos-one">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-6 col-lg-4">
                                                                <div class="demos-one__single">
                                                                    <div class="demos-one__image">
                                                                        <img src="{{ asset('phamkhanhcamp/assets/images/home-showcase/popup-menu-1-1.jpg')}}" alt="eduact" width="416" height="431">
                                                                        <div class="demos-one__buttons">
                                                                            <a href="index.html" class="eduact-btn">
                                                                                <span class="eduact-btn__curve"></span>
                                                                                Multi Page
                                                                            </a>
                                                                            <a href="index-one-page.html" class="eduact-btn">
                                                                                <span class="eduact-btn__curve"></span>
                                                                                One Page
                                                                            </a>
                                                                        </div>
                                                                        <!-- /.demos-one__buttons -->
                                                                    </div><!-- /.demos-one__image -->
                                                                    <div class="demos-one__text">
                                                                        <h3 class="demos-one__text__title">Home Page 01</h3>
                                                                    </div><!-- /.demos-one__text -->
                                                                </div><!-- /.demos-one__single -->
                                                            </div><!-- /.col-md-6 -->
                                                            <div class="col-md-6 col-lg-4">
                                                                <div class="demos-one__single">
                                                                    <div class="demos-one__image">
                                                                        <img src="{{ asset('phamkhanhcamp/assets/images/home-showcase/popup-menu-1-2.jpg')}}" alt="eduact" width="416" height="431">
                                                                        <div class="demos-one__buttons">
                                                                            <a href="index-2.html" class="eduact-btn">
                                                                                <span class="eduact-btn__curve"></span>
                                                                                Multi Page
                                                                            </a>
                                                                            <a href="index-2-one-page.html" class="eduact-btn">
                                                                                <span class="eduact-btn__curve"></span>
                                                                                One Page
                                                                            </a>
                                                                        </div>
                                                                        <!-- /.demos-one__buttons -->
                                                                    </div><!-- /.demos-one__image -->
                                                                    <div class="demos-one__text">
                                                                        <h3 class="demos-one__text__title">Home Page 02</h3>
                                                                    </div><!-- /.demos-one__text -->
                                                                </div><!-- /.demos-one__single -->
                                                            </div><!-- /.col-md-6 -->
                                                            <div class="col-md-6 col-lg-4">
                                                                <div class="demos-one__single">
                                                                    <div class="demos-one__image">
                                                                        <img src="{{ asset('phamkhanhcamp/assets/images/home-showcase/popup-menu-1-3.jpg')}}" alt="eduact" width="416" height="431">
                                                                        <div class="demos-one__buttons">
                                                                            <a href="index-3.html" class="eduact-btn">
                                                                                <span class="eduact-btn__curve"></span>
                                                                                Multi Page
                                                                            </a>
                                                                            <a href="index-3-one-page.html" class="eduact-btn">
                                                                                <span class="eduact-btn__curve"></span>
                                                                                One Page
                                                                            </a>
                                                                        </div>
                                                                        <!-- /.demos-one__buttons -->
                                                                    </div><!-- /.demos-one__image -->
                                                                    <div class="demos-one__text">
                                                                        <h3 class="demos-one__text__title">Home Page 03</h3>
                                                                    </div><!-- /.demos-one__text -->
                                                                </div><!-- /.demos-one__single -->
                                                            </div><!-- /.col-md-6 -->
                                                            <div class="col-md-6 col-lg-4">
                                                                <div class="demos-one__single">
                                                                    <div class="demos-one__image">
                                                                        <img src="{{ asset('phamkhanhcamp/assets/images/home-showcase/popup-menu-1-4.jpg')}}" alt="eduact" width="416" height="431">
                                                                        <div class="demos-one__buttons">
                                                                            <a href="index-dark.html" class="eduact-btn">
                                                                                <span class="eduact-btn__curve"></span>
                                                                                View Page
                                                                            </a>
                                                                        </div>
                                                                        <!-- /.demos-one__buttons -->
                                                                    </div><!-- /.demos-one__image -->
                                                                    <div class="demos-one__text">
                                                                        <h3 class="demos-one__text__title">Home Page Dark</h3>
                                                                    </div><!-- /.demos-one__text -->
                                                                </div><!-- /.demos-one__single -->
                                                            </div><!-- /.col-md-6 -->
                                                            <div class="col-md-6 col-lg-4">
                                                                <div class="demos-one__single">
                                                                    <div class="demos-one__image">
                                                                        <img src="{{ asset('phamkhanhcamp/assets/images/home-showcase/popup-menu-1-5.jpg')}}" alt="eduact" width="416" height="431">
                                                                        <div class="demos-one__buttons">
                                                                            <a href="index-boxed.html" class="eduact-btn">
                                                                                <span class="eduact-btn__curve"></span>
                                                                                View Page
                                                                            </a>
                                                                        </div>
                                                                        <!-- /.demos-one__buttons -->
                                                                    </div><!-- /.demos-one__image -->
                                                                    <div class="demos-one__text">
                                                                        <h3 class="demos-one__text__title">Home Page Boxed</h3>
                                                                    </div><!-- /.demos-one__text -->
                                                                </div><!-- /.demos-one__single -->
                                                            </div><!-- /.col-md-6 -->
                                                            <div class="col-md-6 col-lg-4">
                                                                <div class="demos-one__single">
                                                                    <div class="demos-one__image">
                                                                        <img src="{{ asset('phamkhanhcamp/assets/images/home-showcase/popup-menu-1-6.jpg')}}" alt="eduact" width="416" height="431">
                                                                        <div class="demos-one__buttons">
                                                                            <a href="index-rtl.html#googtrans(en%7car)" class="eduact-btn">
                                                                                <span class="eduact-btn__curve"></span>
                                                                                View Page
                                                                            </a>
                                                                        </div>
                                                                        <!-- /.demos-one__buttons -->
                                                                    </div><!-- /.demos-one__image -->
                                                                    <div class="demos-one__text">
                                                                        <h3 class="demos-one__text__title">Home Page RTL</h3>
                                                                    </div><!-- /.demos-one__text -->
                                                                </div><!-- /.demos-one__single -->
                                                            </div><!-- /.col-md-6 -->
                                                        </div><!-- /.row -->
                                                    </div><!-- /.container -->
                                                </div><!-- /.demos-one -->
                                            </div><!-- /.megamenu-popup__content -->
                                        </div><!-- /.megamenu-popup -->
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#">Pages</a>
                                <ul>
                                    <li><a href="about.html">About</a></li>
                                    <li><a href="team.html">Our Teacher</a></li>
                                    <li><a href="team-carousel.html">Teacher Carousel</a></li>
                                    <li><a href="team-become.html">Become Teacher</a></li>
                                    <li><a href="team-details.html">Teacher Details</a></li>
                                    <li><a href="gallery.html">Gallery</a></li>
                                    <li><a href="gallery-carousel.html">Gallery Carousel</a></li>
                                    <li><a href="pricing.html">Pricing</a></li>
                                    <li><a href="faq.html">FAQs</a></li>
                                    <li><a href="login.html">Login</a></li>
                                    <li><a href="404.html">404 Error</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#">Course</a>
                                <ul>
                                    <li><a href="course.html">Course Page</a></li>
                                    <li><a href="course-carousel.html">Course Carousel</a></li>
                                    <li><a href="management-consulting.html">Management Consulting</a></li>
                                    <li><a href="web-development.html">Web Development</a></li>
                                    <li><a href="frontend-development.html">Frontend Development</a></li>
                                    <li><a href="uiux-design.html">UI/UX Design</a></li>
                                    <li><a href="digital-photography.html">Digital Photography</a></li>
                                    <li><a href="online-business.html">Online Business</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#">Shop</a>
                                <ul class="sub-menu">
                                    <li class="dropdown">
                                        <a href="#">Products</a>
                                        <ul class="sub-menu">
                                            <li><a href="products.html">No Sidebar</a></li>
                                            <li><a href="products-left.html">Left Sidebar</a></li>
                                            <li><a href="products-right.html">Right Sidebar</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="products-carousel.html">Products Carousel</a></li>
                                    <li><a href="product-details.html">Product Details</a></li>
                                    <li><a href="cart.html">Cart</a></li>
                                    <li><a href="checkout.html">Checkout</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#">Blog</a>
                                <ul class="sub-menu">
                                    <li class="dropdown">
                                        <a href="#">Blog Grid</a>
                                        <ul class="sub-menu">
                                            <li><a href="blog-grid.html">No Sidebar</a></li>
                                            <li><a href="blog-grid-left.html">Left Sidebar</a></li>
                                            <li><a href="blog-grid-right.html">Right Sidebar</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#">Blog List</a>
                                        <ul class="sub-menu">
                                            <li><a href="blog-list.html">No Sidebar</a></li>
                                            <li><a href="blog-list-left.html">Left Sidebar</a></li>
                                            <li><a href="blog-list-right.html">Right Sidebar</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="blog-carousel.html">Blog Carousel</a></li>
                                    <li class="dropdown">
                                        <a href="#">Blog Details</a>
                                        <ul class="sub-menu">
                                            <li><a href="blog-details.html">No Sidebar</a></li>
                                            <li><a href="blog-details-left.html">Left Sidebar</a></li>
                                            <li><a href="blog-details-right.html">Right Sidebar</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="contact.html">Contact</a></li>
                        </ul>
                    </div><!-- /.main-menu__nav -->
                    <div class="main-menu__right">
                        <a href="#" class="main-menu__toggler mobile-nav__toggler">
                            <i class="fa fa-bars"></i>
                        </a><!-- /.mobile menu btn -->
                        <a href="#" class="main-menu__search search-toggler">
                            <i class="icon-Search"></i>
                        </a><!-- /.search btn -->
                        <a href="login.html" class="main-menu__login">
                            <i class="icon-account-1"></i>
                        </a><!-- /.login btn -->
                        <div class="main-menu__info">
                            <span class="icon-Call"></span>
                            <a href="tel:3035550105">(303) 555-0105</a>
                            Call to Questions
                        </div>
                        <!-- /.info -->
                    </div><!-- /.main-menu__right -->
                </div><!-- /.container -->
            </nav>
            <!-- /.main-menu -->
        </header><!-- /.main-header-two -->

        <div class="stricky-header stricked-menu main-menu main-header-two">
            <div class="sticky-header__content"></div><!-- /.sticky-header__content -->
        </div><!-- /.stricky-header -->